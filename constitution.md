# 体质

## 体质的数据库字段
*当前字段主要供后端开发时对照，API输出字段名称请参考json格式字段*

    // 数据库字段
    id              id                      体质id
    title           constitutionName        体质标题
    descripe        constitutionSimpleDesc  描述
    content         constitutionDescDetail  详情
    hidden          validTag                是否公开，0 => 未公开，1 => 公开
    priority        orderCode               排序，升序
    createline      createTime              创建时间
    updateline      updateTime              更新时间，时间戳

## json 格式字段
*API获得字段主要参考此处的字段名称和格式*

    // 主要参考该格式
    {
        "id": 1,
        "constitutionName": "",
        "constitutionSimpleDesc": "",
        "constitutionDescDetail": "",
        "validTag": 1,
        "orderCode": 0,
        "createTime": "2013-05-01 13:26:00",
        "updateTime": "2013-05-01 13:26:00",
        // 体质图片的序列
        "constitutionPics": [
            {
                "title": "",
                "coverpath": "http://42.121.118.13:90/upload/images/150x150/2013/18/51806ef156bd7.jpg"
            }
        ]
    }

---

## 获得体质列表

### Request

    url: /api/constitution
    method: POST
    params:
        devId
        userId

### Response

    // 操作成功
    "status": "success",
    "message": "",
    "data": [
        {
            "id": 1,
            "constitutionName": "",
            "constitutionSimpleDesc": ""
            // @link 参考体质相关的json字段
        }
    ]

---

## 获取体质详情

### Request

    url: /api/constitution/detail
    method: POST
    params:
        devId
        userId
        constitutionId

### Response

    // 操作成功
    "status": "success",
    "message": "",
    "data": {
        // @link 参考体质相关的json字段
    }
