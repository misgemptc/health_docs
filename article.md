# 文章

## json 格式字段
*API获得字段主要参考此处的字段名称和格式*

    // 主要参考该格式
    {
        "id": 1,
        "articleTitle": "",
        "articlePic": "",
        "articleContent": "",
        "categoryId": 1,
        "validTag": 1,
        "orderCode": 0,
        "createTime": "2013-05-01 13:26:00",
        "updateTime": "2013-05-01 13:26:00",
        // 废弃字段
        // "articlePicWidth": "",
        // "articlePicHeight": "",

        // 以下两个字段暂时未实现
        // 当前用户是否收藏了该文章，1 => 已收藏，0 => 未收藏，0为默认值
        "favorite": "",
        "userId": 1,
    }

---

## 文章相关的缩略图尺寸定义

    "320x240" => array(320, 240, "both")
    "480x360" => array(480, 360, "both")
    "640x480" => array(640, 480, "both")

---

## 获取文章列表

### Request

    url: /api/article
    method: POST
    params:
        devId
        userId
        categoryId
        favorite
        page            // 页码
        listRows        // 每页显示几行

        // 暂时废弃这两个参数
        devWidth
        devHeight

### Response

    // 操作成功
    "status": "success",
    "message": "",
    "data": [
        {
            "id": 1,
            "articleTitle": "",
            "articlePic": "",

            // 废弃参数
            // "articlePicWidth": 320,
            // "articlePicHeight": 360
        }
    ]

---

## 获取文章详情

### Request

    url: /api/article/detail
    method: POST
    params:
        devId
        userId
        articleId

        // 暂时废弃
        devWidth
        devHeight

### Response

    // 操作成功
    "status": "success",
    "message": "",
    "data": {
        // @link 参考文章相关的json字段
    }

---

## 获取上一篇文章详情

### Request

    url: /api/article/previous
    method: POST
    params:
        devId
        userId
        articleId          // 以该文章为点，查询上一篇文章
        favorite
        devWidth
        devHeight

### Response

    // 操作成功
    "status": "success",
    "message": "",
    "data": {
        // @link 参考文章相关的json字段
    }

---

## 获取下一篇文章详情

### Request

    url: /api/article/next
    method: POST
    params:
        devId
        userId
        articleId          // 以该文章为点，查询下一篇文章
        favorite

        // 暂时废弃
        devWidth
        devHeight

### Response

    // 操作成功
    "status": "success",
    "message": "",
    "data": {
        // @link 参考文章相关的json字段
    }


