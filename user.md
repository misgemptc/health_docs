# 用户

## 接口说明

当创建用户的时候，将用户信息和devId绑定起来，这样的话，会有多个用户是同一个设备号，这个时候可以设置其中一个用户的defaultUser为1，即默认用户

## json 格式字段
*API获得字段主要参考此处的字段名称和格式*

    // 主要参考该格式
    {
        "id": 1,
        "devId": "",                // 32位字符串
        "userName": "",
        "defaultUser": "",          // 1 => 默认用户, 0 => 已注册用户, 默认值为0
        "birthday": "1983-05-01",
        "height": 175,
        "weight": 65,
        "gender": 1,                // 0 => 女性, 1 => 男性, 默认值为0
        "city": "苏州",
        "createTime": "2013-05-01 13:26:00",
        "updateTime": "2013-05-01 13:26:00",
        "validTag": 1,
        "orderCode": 0,
        // 体质 json
        "constitution": {
            "id": "",
            "constitutionName": ""
            // @link 参考体质相关字段
        },
        // 常见病 json
        "disease": [
            {
                "id": 1,
                "diseaseName": ""
                // @link 参考常见病相关字段
            }
        ]
    }

---

## 创建新的用户

### Request

    url: /api/user/create
    method: POST
    params:
        devId           // 设备名，用于将该设备名和用户名关联，同一个设备id下的用户名无法重复
        userName        // 用户名
        defaultUser     // 是否为相同devId下的默认用户，如果将当前用户设置为默认用户，则同devId下的其他用户将自动取消默认用户的设置
        birthday        // 生日，按照 1983-05-01 格式
        height          // 身高
        weight          // 体重
        gender          // 性别, 0 => 女性, 1 => 男性
        city            // 城市名称

### Response

    // 操作成功
    {
        "status": "success",
        "message": "",
        "data": {
            // @link 参考用户json返回字段
        }
    }

---

## 根据devId获取用户列表

### Request

    url: /api/user
    method: POST
    params:
        devId | string | require // 必填，设备号

### Response

    // 操作成功
    {
        "status": "success",
        "message": "",
        "data": [
            {
                "id": "",
                "userName": "",
                "defaultUser": 1
                // 仅返回该三个字段
                // @link 参考用户json返回字段
            }
        ]
    }

---

## 获得指定用户详情

### Request

    url: /api/user/detail
    method: POST
    params:
        devId | string // 当userId不存在时，通过devId查找
        userId | int // 如果存在userId，则优先通过userId查找相关的user表

### Response

    // 操作成功
    {
        "status": "success",
        "message": "",
        "data": {
            // @link 参考用户json返回字段
        }
    }

---

## 更新用户

### Request

    url: /api/user/update
    method: POST
    params:
        devId
        id | int | require          // 必填，这里的id就是userId
        userName
        defaultUser                 // 是否为相同devId下的默认用户，如果将当前用户设置为默认用户，则同devId下的其他用户将自动取消默认用户的设置
        birthday
        height
        weight
        gender
        city
        constitutionId              // 用户体质的id
        diseaseIds                  // 常见病id字符串(1,2,3,6)

### Response

    // 操作成功
    {
        "status": "success",
        "message": "",
        "data": {
            // @link 参考用户json返回字段
        }
    }

---

## 删除用户

### Request

    url: /api/user/delete
    method: POST
    params:
        devId           // 如果当前用户为该devId下的默认用户则无法删除
        userId

### Response

    // 操作成功
    {
        "status": "nothing",
        "message": ""
    }
