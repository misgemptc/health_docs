# 体质测试题

## 体质测试题的数据库字段
*当前字段主要供后端开发时对照，API输出字段名称请参考json格式字段*

    // 数据库字段
    id                  id                  测试题id
    constitution_id     constitutionId      体质id
    title               question            名称
    createline          createTime          创建时间
    updateline          updateTime          更新时间，时间戳
    hidden              validTag            是否公开，0 => 未公开，1 => 公开
    priority            orderCode           排序，升序

## json 格式字段
*API获得字段主要参考此处的字段名称和格式*

    // 主要参考格式
    {
        "id": 1,
        "constitutionId": 2,
        "question": "",
        "createTime": "2013-05-01 13:26:00",
        "updateTime": "2013-05-01 13:26:00",
        "validTag": 1,
        "orderCode": 0
    }

---

## 获取测试题

### Request

    url: /api/question
    method: POST
    params:
        devId
        userId

### Response

    // 操作成功
    {
        "status": "success",
        "message": "",
        "data": {
            // @link 参考体质测试题数据库字段
        }
    }

---

## 提交测试题答案

### Request

    url: /api/question/answer
    method: POST
    params:
        devId       // 设备标示
        userId      // 当前用户标示
        answers     // 答案结果，json格式，参考如下格式

    // answers json格式
    [
        {
            "id": 1,
            "constitutionId": 2,
            "answer": 2
        },
        {
            ...
        }
    ]

### Response

    // 操作成功
    {
        "status": "success",
        "message": "",
        "data": {
            // @link 参考体质数据库字段，返回当前体质的信息
        }
    }